# Production

```bash
## LOCAL

# Generate the app image (must do this one first!)
$ docker build -t git.uwaterloo.ca:5050/mad/mad-booking/madbooking_app_prod -f app.prod.Dockerfile .

# Generate the reverse proxy image
$ docker build -t git.uwaterloo.ca:5050/mad/mad-booking/madbooking_apache_prod -f apache.prod.Dockerfile .


# Push the images
$ docker push git.uwaterloo.ca:5050/mad/mad-booking/madbooking_app_prod
$ docker push git.uwaterloo.ca:5050/mad/mad-booking/madbooking_apache_prod


## ON SERVER

# Take down services
$ docker-compose -f prod.docker-compose.yml down

# Get new images
$ docker-compose -f prod.docker-compose.yml pull

# Start services
$ docker-compose -f prod.docker-compose.yml up -d
```