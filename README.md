# MAD Booking System

## Description
MAD's booking system is used to manage the reservation and signing out of
equipment in a help desk/service desk environment.

Equipment can be tagged for easy categorization.

The table view provides an at-a-glance view of what equipment is currently
available, and the calendar view makes it easy to see when equipment is
available and when it is not. Both views can be filtered down using tags on
equipment.

The system provides management of users including blacklisting. All users are
either admin users or non-admin users. Admin users have full editing rights
and can create, edit, and delete users, tags, equipment, and bookings. Non-admin
users can have equipment booked for them by admin users.

Minimal support for recurring weekly bookings is provided.

This system uses ADFS for authentication and is only tested on UWaterloo's ADFS
infrastructure.

## Developing in Docker Containers

The `Dockerfile` builds a single image that can be used to run the Rails dev
server as well as the webpack dev server.

The simplest way to get up and running in development is:
```bash
$ docker-compose build
$ docker-compose up
```

If the **app** service (the rails container) fails to start with a yarn 
integrity error, you may need to run the following:

```bash
$ docker-compose run app yarn install-check-files
```

That command will install the JS dependencies into the named volume defined in
`docker-compose.yml`.

If the **webpacker** service fails to start, you may need to run a command to
regenerate the executables inside the container:

```bash
$ docker-compose run webpacker bundle exec rails webpacker:binstubs
```

## Docker Deployment in Production

Here are the steps to deploy this software via a docker image.

### Create an OS user
Each version of the application should run under its own OS user that has
been added to the host machine's docker group.

```bash
$ useradd username
$ passwd username
$ usermod -aG docker username
```

### Clone the repository 
Log in as the user created in the previous step. In the location where you want
the code to run:

`$ git clone https://git.uwaterloo.ca/mad/mad-booking.git`

### Provide configuration
There are some configuration files that need to be created. `$APPHOME` represents
the directory with the cloned git repository.

```bash
$ cp $APPHOME/config/database.yml.sample $APPHOME/config/database.yml
$ cp $APPHOME/config/mail_secrets.yml.sample $APPHOME/config/mail_secrets.yml
$ touch $APPHOME/app_vars.env
```
The necessary contents of each file are described below

#### database.yml

Database connection parameters. In our infrastructure we provide these as 
environment variables via the `app_vars.env` file. A sample appears below.

Note: Ensure you have created the `DB_USER` you specify on `DB_HOST`. It must have
`CREATE DATABASE` privileges. Ensure it has the ability to connect to `DB_HOST`
from wherever your docker container is running (for example, by editing
`pg_hba.conf` in postgres).

```yml
  # config/database.yml

  production:
    adapter: postgresql
    encoding: utf8
    reconnect: false
    database: <%= ENV['DB_NAME'] %>
    pool: 5
    username: <%= ENV['DB_USER'] %>
    password: <%= ENV['DB_PASS'] %>
    host: <%= ENV['DB_HOST'] %>
```

#### mail_secrets.yml

This file configures the username and password that will authenticate against
the UWaterloo SMTP server. It is entirely optional to use the email 
functionality of this system. Only fill this in if you need it, and if you trust
everyone that accesses the server. And even then think hard about it.

```yml
  # config/mail_secrets.yml

  mail_username: <%= ENV['MAIL_USERNAME'] %>
  mail_password: <%= ENV['MAIL_PASSWORD'] %>
```

#### app_vars.env

The `prod.docker-compose.yml` file specifies that the contents of app_vars.env
will be available in the `app` container **at runtime** as environment variables.

Provide any variables that are required by the application and its
configuration as show below.

```yml
  # app_vars.env

  # Database connection parameters. These variable names should match what you
  # specified config/database.yml
  DB_HOST=serverhostname
  DB_USER=dbusername
  DB_PASS=dbpassword
  DB_NAME=dbname

  # Email credentials - only specify if needed
  MAIL_USERNAME='username'
  MAIL_PASSWORD='password'
  MAIL_SERVER=smtp.servername.com
  MAIL_PORT=25

  ### Rails application configuration ###

  # Rails env variable for hosting a site in a subfolder
  RAILS_RELATIVE_URL_ROOT=/mad-booking

  # Subfolder configuration for HTML <base href>
  BASE_URL=/mad_booking/

  # Time zone to use for all calendar and date/time views
  TZ=America/Toronto

  # Secret key see: https://guides.rubyonrails.org/security.html#custom-credentials
  SECRET_KEY_BASE=

  # ADFS Stuff
  SLO_URL=https://adfstest.uwaterloo.ca/adfs/ls/?wa=wsignout1.0
  ADFS_METADATA=https://adfstest.uwaterloo.ca/FederationMetadata/2007-06/FederationMetadata.xml
  ADFS_ID=

  # FAST PROJECTOR ID
  PROJECTOR_ID=
```

#### nginx.conf and docker-compose.yml

The supplied `nginx.conf` and `docker-compose.yml` files need to match.
The names of the services in `docker-compose.yml` need to be unique across the
docker network they are running on (i.e. no other `web` or `app` servers).
Also, the `server` directive on line 5 of nginx.conf needs to match the name
of the `app` service (or whatever you called it) in `docker-compose.yml`.

### Build the images

Once the configuration is complete you can build the images with the following 
command

`$ docker-compose -f prod.docker-compose.yml build`

Should that succeed without errors, move on to running the images

### Run containers from the images

Run containers based on the images with the following command

`$ docker-compose -f prod.docker-compose.yml up -d`

Note that the `-d` parameter is short for *detached*. Meaning that the container
will run silently in the background and you will be returned to your command
prompt. If you want to monitor the output of the process, simply omit the `-d` from the command. Note that you will have to press CTRL+C to end the process.

It is likely that this will fail the first time. it will try to start the
application, but the databases don't exist yet. Therefore...

### Create databases

If you've made it this far, you should just need to create the empty database
schema. Do this by running the following commands. They will run the
appropriate rails commands against the application container.

```bash
$ docker-compose -f prod.docker-compose.yml run app rails db:create RAILS_ENV=production
$ docker-compose -f prod.docker-compose.yml run app rails db:migrate RAILS_ENV=production
# Optional:
$ docker-compose -f prod.docker-compose.yml run app rails db:seed RAILS_ENV=production
```
Then start the container

`$ docker-compose -f prod.docker-compose.yml up -d`

### Migrating data

If you need to migrate data from an existing installation, try pgloader.

## Licence

This software is licensed under the MIT licnece. No warranty or support is
provided. See LICENSE.TXT for the details of the licence.

## Authors

[James McCarthy](https://github.com/jmccarth)

[Collin McIntyre](https://github.com/cMack87)

[Kishor Prins](https://github.com/kprinssu)

## Contact

James McCarthy (jmccarth@uwaterloo.ca)
