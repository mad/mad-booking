FROM git.uwaterloo.ca:5050/mad/mad-booking/madbooking_app_prod:latest

FROM httpd

RUN apt-get update && apt-get install -y libapache2-mod-auth-mellon

RUN mkdir -p /usr/local/apache2/conf/sites/
RUN mkdir -p /usr/local/apache2/adfs

COPY FederationMetadata.xml /usr/local/apache2/adfs/FederationMetadata.xml

COPY httpd.conf /usr/local/apache2/conf/httpd.conf
COPY sites.conf /usr/local/apache2/conf/sites/sites.conf


# Copy static assets
COPY --from=0 /app/public /usr/local/apache2/htdocs/mad-booking/
COPY --from=0 /app/public /usr/local/apache2/htdocs/iqc-booking/
COPY --from=0 /app/public /usr/local/apache2/htdocs/ecology-booking/

EXPOSE 443

CMD ["httpd", "-D", "FOREGROUND"]
