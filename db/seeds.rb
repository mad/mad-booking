# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

case Rails.env
when 'development'
  # Create an admin user
  u = User.create(username: 'jmccarth', admin: 'true')

  # Create a default collection
  Collection.create(name: 'GooseCo')
  
  # Create some fake equipment
  (1..1000).to_a.each do
    # Generate random equipment name
    e_name = ""
    (1..12).to_a.each do
      e_name = e_name + rand(65..122).chr
    end

    # Generate random barcode
    e_barcode = ""
    (1..6).to_a.each do
      e_barcode = e_barcode + rand(65..122).chr
    end

    Equipment.create(
      name: e_name,
      barcode: e_barcode
    )
  end

  # Create fake open hours
  OpenHour.create(
    valid_from: Date.today() - 1.year,
    valid_until: Date.today() + 2.year,
    start_time: Time.strptime("00:00","%H:%M"),
    end_time: Time.strptime("23:59","%H:%M")
  )

  # Create some fake events
  (1..100).to_a.each do |e|
    ev = Event.new(
      start: DateTime.now() + e.day,
      end: DateTime.now() + e.day + 1.day
    )

    b = Booking.create(
      user: u,
      equipment: [Equipment.find(e)]
    )

    ev.booking = b
    ev.save!


  end
end
