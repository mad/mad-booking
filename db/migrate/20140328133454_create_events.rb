# frozen_string_literal: true

class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.integer :parent_booking_id
      t.datetime :start
      t.datetime :end

      t.timestamps
    end
  end
end
