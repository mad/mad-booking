# frozen_string_literal: true

class AddContentsToEquipment < ActiveRecord::Migration[5.1]
  def change
    add_column :equipment, :contents, :string
  end
end
