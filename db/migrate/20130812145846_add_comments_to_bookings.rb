# frozen_string_literal: true

class AddCommentsToBookings < ActiveRecord::Migration[5.1]
  def change
    add_column :bookings, :comments, :text
  end
end
