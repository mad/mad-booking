# frozen_string_literal: true

class AddCreatorToBooking < ActiveRecord::Migration[5.1]
  def change
    add_column :bookings, :creator, :string
  end
end
