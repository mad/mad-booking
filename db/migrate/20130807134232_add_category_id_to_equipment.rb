# frozen_string_literal: true

class AddCategoryIdToEquipment < ActiveRecord::Migration[5.1]
  def change
    add_column :equipment, :category_id, :integer
  end
end
