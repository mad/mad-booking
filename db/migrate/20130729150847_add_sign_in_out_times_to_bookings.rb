# frozen_string_literal: true

class AddSignInOutTimesToBookings < ActiveRecord::Migration[5.1]
  def change
    add_column :bookings, :sign_in_times, :text

    add_column :bookings, :sign_out_times, :text
  end
end
