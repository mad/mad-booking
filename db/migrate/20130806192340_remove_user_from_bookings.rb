# frozen_string_literal: true

class RemoveUserFromBookings < ActiveRecord::Migration[5.1]
  def up
    remove_column :bookings, :user
  end

  def down
    add_column :bookings, :user, :string
  end
end
