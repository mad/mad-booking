# frozen_string_literal: true

class RemoveEquipBcFromBookings < ActiveRecord::Migration[5.1]
  def up
    remove_column :bookings, :equip_bc
  end

  def down
    add_column :bookings, :equip_bc, :string
  end
end
