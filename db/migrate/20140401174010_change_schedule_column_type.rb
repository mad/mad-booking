# frozen_string_literal: true

class ChangeScheduleColumnType < ActiveRecord::Migration[5.1]
  def change
    change_column :bookings, :schedule, :text
  end
end
