# frozen_string_literal: true

class AddSelfServeFieldsToBooking < ActiveRecord::Migration[5.1]
  def change
    add_column :bookings, :self_serve, :boolean
    add_column :bookings, :approval_status, :integer
    add_column :bookings, :approver_id, :integer
  end
end
