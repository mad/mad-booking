# frozen_string_literal: true

class AddSerialNumberToEquipment < ActiveRecord::Migration[5.1]
  def change
    add_column :equipment, :serial_number, :string
  end
end
