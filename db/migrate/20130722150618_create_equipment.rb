# frozen_string_literal: true

class CreateEquipment < ActiveRecord::Migration[5.1]
  def change
    create_table :equipment do |t|
      t.string :barcode
      t.string :name
      t.string :stored

      t.timestamps
    end
  end
end
