# Use official, minimal image of Ruby
FROM ruby:3.3.4
# Install essential Linux packages
# RUN apk update && apk add sqlite-dev sqlite postgresql-client postgresql-dev build-base yarn nodejs git  shared-mime-info
RUN curl -sL https://deb.nodesource.com/setup_20.x | bash - && \
      apt-get install -y nodejs build-essential libyaml-dev shared-mime-info git
# Make a working directory
ENV RAILS_ROOT=/app
RUN mkdir -p $RAILS_ROOT
WORKDIR $RAILS_ROOT

# Copy over gemfile and install gems
COPY Gemfile ./Gemfile
COPY Gemfile.lock ./Gemfile.lock
RUN gem install bundler
RUN bundle install

# Copy package.json and install JS dependencies
COPY package.json ./package.json
COPY package-lock.json ./package-lock.json
RUN npm install

# Copy application source
COPY . .

CMD ["bin/rails", "console"]

# CMD ["bundle", "exec", "rake", "db:create"]

# CMD ["bundle", "exec", "rake", "db:migrate", "RAILS_ENV=development"]

# CMD ["bundle", "exec", "rails","server","-b","0.0.0.0"]
