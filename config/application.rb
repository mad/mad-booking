# frozen_string_literal: true

require_relative 'boot'

require 'rails'
require 'active_record/railtie'
# require 'active_storage/engine'
require 'action_controller/railtie'
require 'action_view/railtie'
require 'action_mailer/railtie'
# require 'active_job/railtie'
require 'action_cable/engine'
# require 'action_mailbox/engine'
# require 'action_text/engine'
require 'rails/test_unit/railtie'


# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Equip
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # https://discuss.rubyonrails.org/t/cve-2022-32224-possible-rce-escalation-bug-with-serialized-columns-in-active-record/81017
    config.active_record.yaml_column_permitted_classes = [DateTime, Time, Recurrence, Symbol, Date, SimplesIdeias::Recurrence::Handler::FallBack, SimplesIdeias::Recurrence::Event::Weekly, ActiveSupport::HashWithIndifferentAccess]
    config.active_record.time_zone_aware_types = [:datetime]
    config.time_zone = ENV['TZ']
  end
end
