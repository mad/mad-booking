const { environment } = require('@rails/webpacker')
const webpack = require('webpack')
const VueLoaderPlugin = require('vue-loader/lib/plugin')

const vueLoader = {
  test: /\.vue$/,
  loader: 'vue-loader'
}

const jsLoader = {
  test: /\.js$/,
  loader: 'babel-loader'
}

const cssLoader = {
  test: /\.css$/,
  use: [
    'vue-style-loader',
    'css-loader'
  ]
}

environment.loaders.append('vue',vueLoader);
environment.loaders.append('vue-js',jsLoader);
// environment.loaders.append('vue-css',cssLoader);

environment.plugins.append(
  'Provide',
  new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    'window.jQuery': 'jquery',
    Popper: ['popper.js', 'default']
  })
)

environment.plugins.append(
  'VueLoaderPlugin',
  new VueLoaderPlugin()
)

module.exports = environment
