# Use official, minimal image of Ruby
FROM ruby:3.3.4-alpine

RUN apk update && apk add postgresql-client postgresql-dev build-base nodejs npm shared-mime-info

# Make a working directory
ENV RAILS_ROOT=/app
RUN mkdir -p $RAILS_ROOT
WORKDIR $RAILS_ROOT

# Environment
ENV RAILS_ENV=production
ENV RACK_ENV=production

# Copy over gemfile and install gems
COPY Gemfile ./Gemfile
COPY Gemfile.lock ./Gemfile.lock
RUN gem install bundler
RUN bundle config set without 'development test'
RUN bundle install --jobs 20 --retry 5

# Install JS dependencies
COPY package.json ./package.json
COPY package-lock.json ./package-lock.json
RUN npm install

# Copy app to project
COPY . .

# Precompile
RUN RAILS_ENV=production bundle exec rake assets:precompile

# Start puma
CMD bundle exec puma -C config/puma.rb
