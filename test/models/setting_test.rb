# frozen_string_literal: true

require 'test_helper'

class SettingTest < ActiveSupport::TestCase
  test 'should not save Setting without a key' do
    s = Setting.new
    assert_not s.save
  end

  test 'should not save Setting without a unique key' do
    s = Setting.new(key: 'Default key')
    assert_not s.save
  end
end
