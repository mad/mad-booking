# frozen_string_literal: true

require 'test_helper'

class EventTest < ActiveSupport::TestCase
  test 'valid_open_close? should validate if event is during open hours '\
      'and not closed hours' do
    assert events(:event_1).valid_open_close?
    assert_not events(:event_start_while_closed).valid_open_close?
    assert_not events(:event_end_while_closed).valid_open_close?
  end

  test 'during_open_range? should validate that the event is during '\
       'an open range' do
    assert events(:event_1).during_open_range?
    assert events(:event_start_while_closed).during_open_range?
    assert events(:event_end_while_closed).during_open_range?
    assert_not events(:event_start_end_while_not_open).during_open_range?
    assert_not events(:event_start_while_not_open).during_open_range?
    assert_not events(:event_end_while_not_open).during_open_range?
  end

  test 'during_closed_range? should determine if the event is during '\
       'a closed range' do
    assert_not events(:event_1).during_closed_range?
    assert events(:event_start_closed_hours).during_closed_range?
    assert events(:event_end_closed_hours).during_closed_range?
  end

  test 'open_ranges_start returns an open range when an event overlaps' do
    assert_equal 1, events(:event_1).open_ranges_start.count
    assert_equal 0, events(
      :event_start_end_while_not_open
    ).open_ranges_start.count
    assert_equal 0, events(
      :event_start_while_not_open
    ).open_ranges_start.count
    assert_equal 1, events(
      :event_end_while_not_open
    ).open_ranges_start.count
  end

  test 'open_ranges_end returns an open range when an event overlaps' do
    assert_equal 1, events(:event_1).open_ranges_end.count
    assert_equal 0, events(
      :event_start_end_while_not_open
    ).open_ranges_end.count
    assert_equal 1, events(
      :event_start_while_not_open
    ).open_ranges_end.count
    assert_equal 0, events(
      :event_end_while_not_open
    ).open_ranges_end.count
  end

  test "valid_day_time? returns true when event's day and time are ok" do
    # Day tests
    assert events(:event_1).valid_day_time?
    assert_not events(:event_sunday).valid_day_time?

    # Time tests
    assert_not events(:event_start_while_not_open).valid_day_time?
    assert_not events(:event_end_while_not_open).valid_day_time?
    assert_not events(:event_start_end_while_not_open).valid_day_time?
  end

  test 'current? tells if event is current or not' do
    ev1 = Event.create(
      start: DateTime.now - 1.hour,
      end: DateTime.now + 1.hour
    )

    ev2 = Event.create(
      start: DateTime.now - 1.year,
      end: DateTime.now - 1.year + 1.month
    )

    assert ev1.current?
    assert_not ev2.current?

    ev1.delete
    ev2.delete
  end

  test 'past? tells if event is in the past or not' do
    ev1 = Event.create(
      start: DateTime.now - 1.hour,
      end: DateTime.now + 1.hour
    )

    ev2 = Event.create(
      start: DateTime.now - 1.year,
      end: DateTime.now - 1.year + 1.month
    )

    assert_not ev1.past?
    assert ev2.past?

    ev1.delete
    ev2.delete
  end

  test 'future? tells if event is in the future or not' do
    ev1 = Event.create(
      start: DateTime.now - 1.hour,
      end: DateTime.now + 1.hour
    )

    ev2 = Event.create(
      start: DateTime.now + 1.year,
      end: DateTime.now + 1.year + 1.month
    )

    assert_not ev1.future?
    assert ev2.future?

    ev1.delete
    ev2.delete
  end
end
