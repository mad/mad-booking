# frozen_string_literal: true

require 'test_helper'

class EquipmentTest < ActiveSupport::TestCase
  test 'equipment with no status should default to In' do
    t = Equipment.new(name: 'test item', barcode: 'unique1')
    assert t.save
    assert_equal t.status, 1
  end

  test 'Equipment has correct related Events' do
    skip('event fixtures not yet created')
  end

  test 'Equipment.tag_ids_stringlist returns correct data' do
    e = equipment(:equipment_one)
    t1 = e.tags.first.id.to_s
    t2 = e.tags.last.id.to_s
    assert_equal e.tag_ids_stringlist, "#{t2} #{t1}"
  end

  test 'Equipment.valid_statuses only has 3 items' do
    assert Equipment.valid_statuses.length == 3
  end

  test 'Equipment.get_status returns correct value given a status number' do
    assert Equipment.get_status(0) == 'Out'
    assert Equipment.get_status(1) == 'In'
    assert Equipment.get_status(2) == 'Out for Repair'
    assert Equipment.get_status(3) == 'Invalid Status'
  end
end
