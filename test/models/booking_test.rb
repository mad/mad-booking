# frozen_string_literal: true

require 'test_helper'

class BookingTest < ActiveSupport::TestCase
  test 'basic booking is valid' do
    assert bookings(:booking_1).valid?
  end

  test 'short_description returns a string with details' do
    assert_equal String, bookings(:booking_1).short_description.class
    assert_not bookings(:booking_1).short_description.empty?
  end

  test 'self_serve_description returns a string with details' do
    assert_equal String, bookings(:booking_1).self_serve_description.class
    assert_not bookings(:booking_1).self_serve_description.empty?
  end

  test 'valid_statuses only has 3 possible statuses' do
    assert_equal 3, Booking.valid_statuses.count
  end

  test 'get_status returns a status label for values 0 to 2, '\
       'invalid otherwise' do
    (0..2).each do |i|
      assert_equal String, Booking.get_status(i).class
      assert_not Booking.get_status(i).empty?
    end

    assert_equal 'Invalid Status', Booking.get_status(3)
  end

  test 'get_most_recent_event returns an appropriate event' do
    assert_equal events(:event_now),
                 bookings(:booking_today).get_most_recent_event
  end

  test 'is_active returns true when equipment is signed out, '\
       'or false otherwise' do
    assert bookings(:booking_out).is_active?
    assert_not bookings(:booking_1).is_active?
    assert_not bookings(:booking_out_in).is_active?
  end

  test 'is_overdue returns true when equipment is overdue, '\
       'or false otherwise' do
    equip = equipment(:equipment_one)
    overdue_booking = Booking.create(
      user: users(:user_regular),
      creator: users(:user_admin),
      self_serve: false,
      equipment: [equip],
      events: [events(:event_1)],
      sign_out_times: {
        equip.id => DateTime.now - 1.hour
      }
    )

    assert overdue_booking.is_overdue?
    assert_not bookings(:booking_out).is_overdue?
    assert_not bookings(:booking_1).is_overdue?
    assert_not bookings(:booking_out_in).is_overdue?
  end

  test 'get_item_status returns an appropriate status for various cases' do
    equip = equipment(:equipment_one)
    overdue_booking = Booking.create(
      user: users(:user_regular),
      creator: users(:user_admin),
      self_serve: false,
      equipment: [equip],
      events: [events(:event_1)],
      sign_out_times: {
        equip.id => DateTime.now - 1.hour
      }
    )

    assert_equal 'Booked', bookings(:booking_1).get_item_status(equip.id)
    assert_equal 'Out', bookings(:booking_out).get_item_status(equip.id)
    assert_equal 'In', bookings(:booking_out_in).get_item_status(equip.id)
    assert_equal 'Overdue', overdue_booking.get_item_status(equip.id)
  end

  ### Testing validators ###
  test 'real_user only validates bookings where user already exists' do
    booking = Booking.create(
      creator: users(:user_admin),
      self_serve: false,
      events: [events(:event_1)]
    )
    assert_not booking.valid?

    booking.delete
  end

  test 'allowed_user only allows bookings for users in good standing' do
    equip = equipment(:equipment_two)

    # greylisted users can still make bookings
    booking = Booking.create(
      user: users(:user_greylisted),
      creator: users(:user_admin),
      self_serve: false,
      events: [events(:event_1)],
      equipment: [equip]
    )
    assert booking.valid?
    booking.delete

    # blacklisted users cannot make bookings
    booking2 = Booking.create(
      user: users(:user_blacklisted),
      creator: users(:user_admin),
      self_serve: false,
      events: [events(:event_1)],
      equipment: [equip]
    )
    assert_not booking2.valid?
    booking2.delete
  end

  test 'correct_times validates that start time is before end time' do
    equip = equipment(:equipment_two)
    booking = Booking.create(
      user: users(:user_greylisted),
      creator: users(:user_admin),
      self_serve: false,
      events: [
        Event.create(
          start: DateTime.now,
          end: DateTime.now - 1.day
        )
      ],
      equipment: [equip]
    )
    assert_not booking.valid?
    booking.delete
  end

  test "not_selfserve_past doesn't allow self serve bookings in the past" do
    equip = equipment(:equipment_two)
    booking = Booking.create(
      user: users(:user_greylisted),
      creator: users(:user_admin),
      self_serve: true,
      events: [
        Event.create(
          start: DateTime.now - 1.day,
          end: DateTime.now - 2.day
        )
      ],
      equipment: [equip]
    )
    assert_not booking.valid?
    booking.delete
  end

  test 'booking that conflicts with booking_1 is invalid' do
    conflict_booking = Booking.create(
      user: users(:user_regular),
      creator: users(:user_admin),
      self_serve: false,
      equipment: [equipment(:equipment_one)],
      events: [events(:event_1)]
    )
    assert_not conflict_booking.valid?
  end
end
