# frozen_string_literal: true

require 'test_helper'
require 'date'

class OpenHourTest < ActiveSupport::TestCase
  test 'OpenHour is valid' do
    assert open_hours(:open_hour_valid).valid?
  end

  test 'OpenHour validates correct_times' do
    oh = OpenHour.create(
      valid_from: Date.today,
      valid_until: Date.today - 1.day,
      start_time: Time.utc(2019, 1, 1, 8, 30, 0),
      end_time: Time.utc(2019, 1, 1, 12, 30, 0)
    )
    assert_not oh.valid?
    oh.delete

    oh2 = OpenHour.create(
      valid_from: Date.today,
      valid_until: Date.today + 1.year,
      start_time: Time.utc(2019, 1, 1, 12, 30, 0),
      end_time: Time.utc(2019, 1, 1, 8, 30, 0)
    )
    assert_not oh2.valid?
    oh2.delete
  end

  test 'open_times_from_date returns an array of open times' do
    assert_equal 1, OpenHour.open_times_from_date(Date.new(2019, 6, 6)).count
  end
end
