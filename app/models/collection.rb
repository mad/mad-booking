# frozen_string_literal: true

# Collection holds the name and settings for the collection being managed
# through the booking system.
class Collection < ApplicationRecord
  has_settings do |s|
    s.key :collection, defaults: { name: 'Booking System' }
    s.key :self_serve, defaults: {
      allow: "0",
      max_length: 72,
      enforce_dept: "1"
    }
    s.key :validation, defaults: {
      watiam: "1",
      departments: ''
    }
    s.key :email, defaults: {
      from_address: '',
      template_booking_from: '',
      template_salutation: '',
      on_sign_in: "0",
      on_sign_out: "0",
      on_booking: "0",
      on_approval: "0",
      on_rejection: "0",
      on_booking_edit: "0",
      on_booking_delete: "0",
      on_self_serve_booking: "0",
      on_self_serve_edit: "0",
      on_self_serve_delete: "0"
    }
  end
end
