import SignInOut from './SignInOut.vue';
import { createApp } from 'vue';

document.addEventListener('DOMContentLoaded', () => {
  createApp(SignInOut).mount('#sign_in_out');
  // new Vue({
  //   render: h => h(SignInOut)
  // }).$mount('#sign_in_out');
});