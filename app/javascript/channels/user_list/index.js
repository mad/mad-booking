
document.addEventListener('DOMContentLoaded', () => {
    let search_filter = document.getElementById("clearSearchFilters");
    if (search_filter != null){
      search_filter.onclick = function() {
        // when removing the search filter, clear the text box
        document.getElementById("search_all").value = "";
      }
    }
});