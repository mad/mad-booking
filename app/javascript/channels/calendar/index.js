import Calendar from './Calendar.vue';
import { createApp } from 'vue';

document.addEventListener('DOMContentLoaded', () => {

  createApp(Calendar).mount('#calendar');
  // new Vue({
  //   render: h => h(Calendar)
  // }).$mount('#calendar');

});