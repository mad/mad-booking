import Dashboard from './Dashboard.vue';
import { createApp } from 'vue';

document.addEventListener('DOMContentLoaded', () => {

  createApp(Dashboard).mount('#dashboard');
  // new Vue({
  //   render: h => h(Dashboard)
  // }).$mount('#dashboard');

});