import BookingForm from './BookingForm.vue';
import { createApp } from 'vue';

document.addEventListener('DOMContentLoaded', () => {

  createApp(BookingForm).mount('#booking_form');
  // new Vue({
  //   render: h => h(BookingForm)
  // }).$mount('#booking_form');
});