
document.addEventListener('DOMContentLoaded', () => {
    let search_filter = document.getElementById("search_filter");
    if (search_filter != null){
      search_filter.onclick = function() {
        // when removing the search filter, clear the text box and
        document.getElementById("search_all").value = "";
        
        // remove the filter button
        search_filter.remove();
      }
    }

    let tag_filter = document.getElementById("tag_filter");
    if (tag_filter != null){
      tag_filter.onclick = function() {
        // when removing the tag filter, remove all checks from boxes
        let tags = document.querySelectorAll(".form-check-input[name='tag_ids[]']");
        tags.forEach(element => {
          element.checked = false;
        });

        // once all elements are unchecked, destroy the tag filter button
        tag_filter.remove();
      }
    }

    let availability_filter = document.getElementById("availability_filter");
    availability_filter.onclick = function(){
      document.getElementById("start_date").value = "";
      document.getElementById("end_date").value = "";
      document.getElementById("start_time").value = "";
      document.getElementById("end_time").value = "";

      availability_filter.remove();
    }
});