# frozen_string_literal: true

# ApplicationHelper module
module ApplicationHelper
  def report_errors(object)
    content_tag :div, class: 'alert-box alert' do
      content_tag :ul do
        object.errors.full_messages.collect do |error|
          concat(content_tag(:li, error))
        end
      end
    end
  end
end
