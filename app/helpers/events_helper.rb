# frozen_string_literal: true

# EventsHelper is used to help manipulate events in the controller
module EventsHelper
  def find_by_date_range(start_date, end_date, tag_list, equip_list)
    # Retrieve all events that do not start after end_date
    # and do not end before start_date
    range_events = Event.includes(booking: [:user, equipment: [:tags]]).where(
      'start <= ? AND "end" >= ?',
      end_date,
      start_date
    )
    # If there is an equipment list parameter with values, add that condition
    # to the query
    unless equip_list.empty?
      range_events = range_events.where("bookings_equipment.equipment_id": equip_list)
    end

    # If there is a tag list parameter with values, add that condition to the
    # query
    unless tag_list.empty?
      range_events = range_events.where("equipment_tags.tag_id": tag_list)
    end
    range_events
  end

  def find_upcoming_events(start_date, num_days)
    # Retrieve all events that start on or after the start date for the
    # specified number of days.
    range_events = Booking.joins(:events).where(
      'start between ? AND ?',
      start_date - 1.day,
      start_date + num_days
    ).order('"end"')
    range_events
  end

  # Return a list of unique equipment tags associated with the equipment
  # for this booking
  def prep_tags(ev_booking)
    ev_booking.equipment.flat_map(&:tag_ids).uniq
  end

  # Build a list of equipment that will be displayed in the calendar
  # event for the booking
  def build_equip_list(evb_equip)
    equip_list = ''
    if evb_equip.length <= 5
      evb_equip.each do |e|
        equip_list += e.name + '; '
      end
    else
      equip_list += build_multi_item_equip_list(evb_equip)
    end

    equip_list
  end

  # Build a list of equipment that will be displayed in the calendar
  # event for the booking, in the case where there are more than 5 items
  def build_multi_item_equip_list(evb_equip)
    equip_list = ''
    evb_equip[0..3].each do |e|
      equip_list += e.name + ';'
    end
    equip_list += ' & ' + (evb_equip.length - 4).to_s + ' more items'
  end

  # Build an HTML element to show the user on the calendar event
  def build_user_info(ev_booking, evb_user)
    user = evb_user.nil? ? 'UNKNOWN USER' : evb_user.username

    # unless ev_booking.schedule.nil?
    #   user += '<span class="right"><i class="fas fa-redo-alt"></i></span>'
    # end
    user
  end

  # Build an HTML element to show the title of the calendar event
  def build_title(user, equip_list)
    user + ' ' + equip_list
  end

  def convert_to_calendar_event(event)
    if event.booking
      ev_booking = event.booking
      evb_user = ev_booking.user
      evb_equip = ev_booking.equipment

      # convert to Full Calendar's ISO8601 format
      event_start_str = event.start.iso8601
      event_end_str = event.end.iso8601

      # get tags
      evb_tags = prep_tags(ev_booking)

      # user information
      user = build_user_info(ev_booking, evb_user)

      # equipment information
      equip_list = build_equip_list(evb_equip)

      # create title
      b_title = build_title(user, equip_list)

      b_status = 0
      # b_color = '#9933cc' #purple
      b_color = '#d8b3e3' #purple
      num_items_in = 0
      evb_equip_statuses = []

      # Equipment status
      evb_equip.each do |e|
        # Pending = 0
        # Active = 1
        # Done = 2
        # Overdue = 3

        item_status = ev_booking.get_item_status(e.id)
        evb_equip_statuses.push(item_status)
        if item_status == 'Out'
          # If at least 1 item is "Out" the booking is active
          b_status = 1
          # b_color = '#3366ff' #blue
          b_color = '#76bcc4' #blue
        elsif item_status == 'Overdue'
          # If at least 1 item is "Overdue" the booking is overdue
          b_status = 3
          # b_color = '#dd0000' #red
          b_color = '#ff7c7c' #red
        elsif item_status == 'In'
          num_items_in += 1
        end
      end

      # If b_status is still 0, then all items are "In" or "Booked"
      # If number of items In matches number of items in the booking,
      # it is finished if non-recurring
      # Or if it is recurring and the last event has ended
      if  (num_items_in == evb_equip.length) &&
          (ev_booking.schedule.nil? || ev_booking.events.last.end <= Time.now)
        b_status = 2
        b_color = '#b2e593' #green
      end

      # If event is pending, override the color to grey
      if ev_booking.approval_status == 0
        # b_color = '#a9a9a9' #grey
        b_color = '#d2d2d2' #grey
      elsif ev_booking.approval_status == 2
        b_color = '#ff4500' #orangered
      end

      # Determine if the event spans days or not
      if event.start.year == event.end.year && event.start.yday == event.end.yday
        b_all_day = false
      else
        b_all_day = true
      end

      {
        title: '',
        start: event_start_str,
        end: event_end_str,
        id: ev_booking.id,
        status: b_status,
        borderColor: '#686868',
        color: b_color,
        backgroundColor: b_color,
        textColor: '#000000',
        equip: evb_equip,
        tags: evb_tags,
        equip_status: evb_equip_statuses,
        username: evb_user.username,
        allDay: b_all_day
      }
    else
      {}
    end
  end
end
