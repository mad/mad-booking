# frozen_string_literal: true

# UserMailer controls different mailing tasks the application needs to do
class UserMailer < ActionMailer::Base
  collection = Collection.first
  unless collection.settings(:email).from_address == ''
    default from: collection.settings(:email).from_address
  end

  # Admin action emails
  def admin_approved_email(user, booking)
    collection = Collection.first
    @user = user
    @booking = booking
    email = user.email_address
    mail(to: email, subject: "APPROVED - Equipment Booking at #{collection.settings(:email).template_booking_from}")
  end

  def admin_rejected_email(user, booking)
    collection = Collection.first
    @user = user
    @booking = booking
    email = user.email_address
    mail(to: email, subject: "REJECTED - Equipment Booking at #{collection.settings(:email).template_booking_from}")
  end

  def admin_create_email(user, booking)
    collection = Collection.first
    @user = user
    @booking = booking
    email = user.email_address
    mail(to: email, subject: "Equipment Booking at #{collection.settings(:email).template_booking_from}")
  end

  def admin_edit_email(user, booking)
    collection = Collection.first
    @user = user
    @booking = booking
    email = user.email_address
    mail(to: email, subject: "Equipment Booking at #{collection.settings(:email).template_booking_from}")
  end

  def admin_sign_out_email(user, booking, eq_ids)
    collection = Collection.first
    @user = user
    @booking = booking
    @eq_ids = eq_ids
    email = user.email_address
    mail(to: email, subject: "Equipment Signed Out at #{collection.settings(:email).template_booking_from}")
  end

  def admin_sign_in_email(user, booking, eq_ids)
    collection = Collection.first
    @user = user
    @booking = booking
    @eq_ids = eq_ids
    email = user.email_address
    mail(to: email, subject: "Equipment Signed In at #{collection.settings(:email).template_booking_from}")
  end

  def admin_delete_email(user, booking)
    collection = Collection.first
    @user = user
    @booking = booking
    email = user.email_address
    mail(to: email, subject: "Reservation cancelled at #{collection.settings(:email).template_booking_from}")
  end

  # Self serve action emails
  def self_create_email(user, booking)
    collection = Collection.first
    @user = user
    @booking = booking
    email = user.email_address
    mail(to: email, subject: "PENDING - Equipment Booking at #{collection.settings(:email).template_booking_from}")
  end

  def self_edit_email(user, booking)
    collection = Collection.first
    @user = user
    @booking = booking
    email = user.email_address
    mail(to: email, subject: "Equipment Booking at #{collection.settings(:email).template_booking_from}")
  end

  def self_delete_email(user, booking)
    collection = Collection.first
    @user = user
    @booking = booking
    email = user.email_address
    mail(to: email, subject: "Equipment Booking at #{collection.settings(:email).template_booking_from}")
  end
end
