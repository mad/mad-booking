# frozen_string_literal: true

class ClosedHoursController < ApplicationController
  before_action :set_closed_hour, only: %i[show edit update destroy]

  before_action :authenticate
  before_action :authenticate_admin

  # GET /closed_hours
  def index
    @closed_hours = ClosedHour.all
  end

  # GET /closed_hours/1
  def show; end

  # GET /closed_hours/new
  def new
    @closed_hour = ClosedHour.new
  end

  # GET /closed_hours/1/edit
  def edit; end

  # POST /closed_hours
  def create
    # @closed_hour = ClosedHour.new(closed_hour_params)
    closed_hour_start = params[:closed_hour_start_date] + 'T' + params[:closed_hour_start_time]
    closed_hour_end = params[:closed_hour_end_date] + 'T' + params[:closed_hour_end_time]
    
    @closed_hour = ClosedHour.new(
      closed_start: closed_hour_start,
      closed_end: closed_hour_end
    )
    if @closed_hour.save
      redirect_to closed_hours_path, notice: 'Closed hour was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /closed_hours/1
  def update
    if @closed_hour.update(closed_hour_params)
      redirect_to closed_hours_path, notice: 'Closed hour was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /closed_hours/1
  def destroy
    @closed_hour.destroy
    redirect_to closed_hours_url, notice: 'Closed hour was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_closed_hour
    @closed_hour = ClosedHour.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def closed_hour_params
    params.require(:closed_hour).permit(:closed_start, :closed_end)
  end
end
