# frozen_string_literal: true
require 'will_paginate/array'
# UsersController manages CRUD operations for Users
class UsersController < ApplicationController
  # before_action :authenticate_user! do |controller|
  #   controller.admin_user()
  # end

  before_action :authenticate
  before_action :authenticate_admin, except: %i[show]

  def form
    @user_id = params[:userid]

    @user = @user_id.nil? || /^[0-9]+$/.match(@user_id).nil? ? User.new : User.find(@user_id)

    respond_to do |format|
      format.js
    end
  end

  def validate_dept
    @user = User.find_by(username: params[:userid])
    respond_to do |format|
      format.json { render json: @user.validate_department }
    end
  end

  def ldap
    @user = User.find_by(username: params[:userid])
    @details = if @user.nil?
                 nil
               else
                 @user.ldap_details
               end

    respond_to do |format|
      format.json { render json: @details }
    end
  end

  def valid_ldap_user
    valid = User.ldap_user?(params[:username])

    respond_to do |format|
      format.json { render json: valid }
    end
  end

  def column
    if request.params[:term]
      @users = User.where('username LIKE ?', '%' + request.params[:term] + '%').pluck(request.params[:column])
    else
      @users = User.pluck(request.params[:column])
    end
    respond_to do |format|
      format.json { render json: @users }
    end
  end

  def ldap_search
    users = if request.params[:username]
              User.ldap_search(request.params[:username])
            else
              []
            end
    respond_to do |format|
      # format.json { render json: users }
      format.json do
        render json: users.map { |user|
                       u = User.find_by_username(user[:uid].first)
                       status = u.nil? ? 0 : u.status
                       comments = u.nil? ? '' : u.comments
                       id = u.nil? ? -1 : u.id
                       {
                         username: user[:uid].first,
                         ou: user[:ou].join(','),
                         status: status,
                         comments: comments,
                         user_id: id
                       }
                     }
      end
    end
  end

  # GET /users
  # GET /users.json
  def index
    page_size = 10
    if params[:per_page].present?
      page_size = params[:per_page] == 'all' ? 0 : params[:per_page].to_i
    end
    if request.params[:sort_order]
      sort_order = request.params[:sort_order]
    else
      sort_order = "ASC"
    end
    @users = if request.params[:search]
               User.where('username LIKE ?', "%#{request.params[:search]}%").order("username #{sort_order}").paginate(page: params[:page], per_page: page_size)
             else
               User.all.order("username #{sort_order}").paginate(page: params[:page], per_page: page_size)
             end


    respond_to do |format|
      format.html # index.html.erb
      if @users.count > 10
        format.json { render json: @users }
      else
        format.json { render json: @users.map { |user| user.attributes.merge(ldap_details: user.ldap_details) } }
      end
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])
    logged_in_user = User.find_by_username(session[:current_username])

    respond_to do |format|
      if (logged_in_user.admin)
        # Admins can see information about anyone
        format.html # show.html.erb
        format.json { render json: @user }
      else
        # Non-admin users should only be able to get information about themselves
        if (@user.username == logged_in_user.username)
          format.html # show.html.erb
          format.json { render json: @user }
        else
          format.html # show.html.erb
          format.json { render json: {}, status: 401 }
        end
      end
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
  end

  # POST /users
  # POST /users.json
  def create
    up = user_params
    up[:username] = up[:username].strip
    @user = User.new(up)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])

    up = user_params
    up[:username] = up[:username].strip
    
    respond_to do |format|
      if @user.update(up)
        format.html { redirect_to root_path, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to root_path, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def user_params
    params.require(:user).permit(:username, :admin, :status, :comments, :firstname, :lastname)
  end
end
