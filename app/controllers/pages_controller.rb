# frozen_string_literal: true

# PagesController is used to route to static pages not attached to specific
# models
class PagesController < ApplicationController
  before_action :authenticate
  before_action :authenticate_admin, except: %i[invaliduser user_history selfserve]
  before_action :authenticate_department, only: [:selfserve]

  # before_action :check_settings

  def home; end

  def home2; end

  def calendar_new; end

  def invaliduser; end

  def selfserve; end

  def selfserve_success; end

  def selfserve_fail; end

  def selfserve_disabled; end

  def user_history; end
end
