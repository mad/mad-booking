# frozen_string_literal: true

# Application Controller does some high level validation to make sure
# the application has all required settings before beginning
# and enforces permissions on who can use the application and
# the self serve functionality
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :http_remote_user

  # def is_admin_user()
  #   User.where(username: session[:current_username], admin: true).length > 0
  # end

  def http_remote_user
    if ENV['REMOTE_USER']
      ENV['REMOTE_USER']
    else
      request.env['HTTP_REMOTE_USER'] || request.headers['X-Forwarded-User']
    end
  end

  def authenticate
    # If there is no logged in user then redirect to login
    # Need to force loading of session as it is lazy-loaded
    session[:init] = true
    unless session[:current_username].present?
      # redirect_to root_url + 'auth/cas?origin=' + request.original_url
      if ENV['RAILS_RELATIVE_URL_ROOT'].present?
        redirect_to ENV['RAILS_RELATIVE_URL_ROOT'] + '/auth/create'
      else
        redirect_to '/auth/create'
      end
    end
  end

  def authenticate_admin
    unless User.admin_user?(session[:current_username])
      flash[:message] = 'User ' + session[:current_username] + ' does not have permission to use the booking system as an administrator.'
      flash[:error_type] = 'NonAdmin'
      redirect_to selfserve_path
    end
  end

  def authenticate_department
    collection = Collection.first
    if collection.settings(:self_serve).enforce_dept == "1"
      unless User.find_by_username(session[:current_username]).validate_department
        flash[:message] = 'User ' + session[:current_username] + ' is from a department not granted access to self serve bookings'
        flash[:error_type] = 'InvalidDept'
        redirect_to invalid_user_path
      end
    end
  end

  def admin_user
    if User.where(username: current_user.username, admin: true).empty?
      # unless User.where(username: current_user.username, admin: true).length > 0
      flash[:notice] = 'User ' + current_user.username + ' does not have permission to use the booking system as an admin.'
      redirect_to invalid_user_path
    end
  end

  def valid_user
    collection = Collection.first
    unless User.ldap_user?(current_user.username)
      flash[:notice] = "#{current_user.username} cannot use the booking system. You cannot be found in LDAP or #{current_user.username} is not a valid LDAP account. Please contact #{collection.settings(:email).from_address} for assistance."
      redirect_to invalid_user_path
    end
  end

  def check_self_serve
    collection = Collection.first
    if collection.settings(:self_serve).allow != "1"
      flash[:alert] = 'Self serve not enabled. Check your settings.'.html_safe
      redirect_to selfserve_disabled_path
    end
  end

  def login_route
    if ENV['RAILS_RELATIVE_URL_ROOT'].present?
      ENV['RAILS_RELATIVE_URL_ROOT'] + '/auth/saml'
    else
      '/auth/saml'
    end
  end
end
