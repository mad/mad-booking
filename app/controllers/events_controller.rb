# frozen_string_literal: true

include EventsHelper
# EventsController manages CRUD operations for Events
class EventsController < ApplicationController
  before_action :set_event, only: %i[show edit update destroy]

  helper_method :find_by_date_range

  # before_action :authenticate_user! do |controller|
  #   controller.admin_user()
  # end

  before_action :authenticate
  before_action :authenticate_admin


  def calendar
    # provided params :start, :end are in 'YYYY-MM-DD' format
    # convert both to a Ruby Time object
    sTime = Date.parse(params[:start]).to_time
    eTime = Date.parse(params[:end]).to_time

    # if equipment ids are sent as a filter, pass it to the find_by_date_range
    # function so it can filter on the query
    if params.key?(:equip_ids)
      equip_ids = params[:equip_ids]
      @events = find_by_date_range(sTime, eTime, [], equip_ids)
    elsif params.key?(:tag_ids)
      tag_ids = params[:tag_ids]
      @events = find_by_date_range(sTime, eTime, tag_ids, [])
    else
      @events = find_by_date_range(sTime, eTime, [], [])
    end

    # if user id is sent as a filter, apply it
    if params.key?(:user_id)
      @events = @events.includes(:booking).where(users: { id: params[:user_id] }).references(:user)
    end

    b = [] # array of fullcalendar events

    # only show event if it's booking isn't rejected
    @events.each do |ev|
      b.push(convert_to_calendar_event(ev)) if ev.booking.approval_status != 2
    end

    respond_to do |format|
      format.json { render json: b }
    end
  end

  # GET /events
  def index
    @events = Event.all
  end

  # GET /events/1
  def show; end

  # GET /events/new
  def new
    @event = Event.new
  end

  # GET /events/1/edit
  def edit; end

  # POST /events
  def create
    @event = Event.new(event_params)

    if @event.save
      redirect_to @event, notice: 'Event was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /events/1
  def update
    if @event.update(event_params)
      redirect_to @event, notice: 'Event was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /events/1
  def destroy
    @event.destroy
    redirect_to events_url, notice: 'Event was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_event
    @event = Event.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def event_params
    params.require(:event).permit(:parent_booking_id, :start, :end)
  end
end
