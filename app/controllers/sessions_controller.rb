# frozen_string_literal: true

class SessionsController < ApplicationController
  protect_from_forgery with: :exception, except: [:create]

  def create
    # uname = auth_hash[:uid]
    uname = http_remote_user
    @user = User.find_or_create_from_username(uname)
    session[:current_username] = @user.username unless @user.nil?
    redirect_to root_path
  end

  def destroy
    session[:current_username] = nil
    @user = nil
    redirect_to ENV['SLO_URL']
  end

  def failure
    flash[:notice] = params[:message]
    redirect_to '/'
  end

  protected

  def auth_hash
    request.env['omniauth.auth']
  end
end
