# frozen_string_literal: true

# TagsController manages CRUD operations for tags
class TagsController < ApplicationController
  before_action :set_tag, only: %i[show edit update destroy]

  before_action :authenticate
  before_action :authenticate_admin, except: %i[index]

  # /tags/form
  def form
    @tag_id = params[:tagid]
    @tag = @tag_id.nil? || /^[0-9]+$/.match(@tag_id).nil? ? Tag.new : Tag.find(@tag_id)

    respond_to do |format|
      format.js
    end
  end

  # GET /tags
  # GET /tags.json
  def index
    page_size = 10
    if params[:per_page].present?
      page_size = params[:per_page] == 'all' ? Tag.count : params[:per_page].to_i
    end
    @tags = Tag.all

    @tags = if request.params[:search]
              Tag.where('name LIKE ?', "%#{request.params[:search]}%").paginate(page: params[:page], per_page: page_size)
            else
              Tag.all.paginate(page: params[:page], per_page: page_size)
            end

    respond_to do |format|
      format.html # index.html.erb
      # format.json { render json: @tags }
      format.json { render json: @tags.to_json }
    end
  end

  # GET /tags/1
  def show
    @tag = Tag.find(params[:id])

    respond_to do |format|
      format.html
      format.json { render json: @tag.to_json }
    end
  end

  # GET /tags/new
  def new
    @tag = Tag.new
  end

  # GET /tags/1/edit
  def edit; end

  # POST /tags
  def create
    @tag = Tag.new(tag_params)

    if @tag.save
      redirect_to @tag, notice: 'Tag was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /tags/1
  def update
    if @tag.update(tag_params)
      redirect_to @tag, notice: 'Tag was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /tags/1
  def destroy
    @tag.destroy
    redirect_to tags_url, notice: 'Tag was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_tag
    @tag = Tag.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def tag_params
    params.require(:tag).permit(:name)
  end
end
