# frozen_string_literal: true

# EquipmentController manages the CRUD operations for equipment and provides
# some API functionality to check for available/unavailable equipment
# for a given time range
class EquipmentController < ApplicationController
  # before_action :authenticate_user!, except: [:unavailable_time_range] do |controller|
  #     controller.admin_user()
  # end

  # before_action only: [:unavailable_time_range] do |controller|
  #   controller.valid_user()
  # end

  before_action :authenticate
  before_action :authenticate_admin, except: [:unavailable_time_range, :index]

  def summary_view
    @equipment = Equipment.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @equipment }
      format.js
    end
  end

  # return list of unavailable equipment IDs given a start and end datetime
  def unavailable_time_range
    e_ids = []
    st = params[:start_time]
    et = params[:end_time]
    sd = params[:start_date]
    ed = params[:end_date]
    event_start_dt = Time.strptime(sd + st, '%m/%d/%Y%I:%M %p')
    event_end_dt = Time.strptime(ed + et, '%m/%d/%Y%I:%M %p')
    # e_ids = Event.includes(:booking).where("start >= ? AND end <= ?", event_start_dt, event_end_dt).map { |ev| ev.booking.equipment_ids }.flatten.uniq
    events = Event.includes(:booking).where('start >= ? AND "end" <= ?', event_start_dt, event_end_dt)
    events.each do |ev|
      ev.booking.equipment_ids.each do |eid|
        e_ids.push(eid) if ev.booking.get_item_status(eid) != 'In'
      end
    end
    respond_to do |format|
      format.json { render json: e_ids }
    end
  end

  def form
    @equip_id = params[:equip_id]
    @equipment = @equip_id.nil? || /^[0-9]+$/.match(@equip_id).nil? ? Equipment.new : Equipment.find(@equip_id)

    respond_to do |format|
      format.js
    end
  end

  # GET /equipment
  # GET /equipment.json
  def column
    @equipment = Equipment.pluck(request.params[:column])

    respond_to do |format|
      format.json { render json: @equipment }
    end
  end

  def tags
    @tagged_equipment = Equipment.joins(:tags).where(tags: { id: request.params[:tag_id] })

    respond_to do |format|
      format.js
      format.json
    end
  end

  # GET /equipment.json
  def index
    page_size = 10
    if params[:per_page].present?
      page_size = params[:per_page] == 'all' ? 0 : params[:per_page].to_i
    end
    @equipment = Equipment.includes(:tags).all.paginate(page: params[:page], per_page: page_size)

    # Filter by Tag ID
    if params[:tag_ids].present?
      @equipment = @equipment.joins(:tags).where(tags: { id: request.params[:tag_ids] })
    end

    # Filter by search term
    if params[:search_term].present?
      @equipment = @equipment.search_equipment(params[:search_term])
    end

    # Filter by exclude id
    if params[:exclude_ids].present?
      @equipment = @equipment.where.not(id: request.params[:exclude_ids])
    end
    # Check that time filter params are passed and have a value
    if params[:start_date_time].present? && params[:end_date_time].present?
      st = Time.strptime(params[:start_date_time], '%Y-%m-%dT%H:%M')
      et = Time.strptime(params[:end_date_time], '%Y-%m-%dT%H:%M')
      # find a list of equipment with conflicting bookings
      conflict_equipment = @equipment.joins(bookings: { events: :booking }).where('start <= ? AND "end" >= ?', et, st)
      # get the unique ids from that list
      unavailable_ids = conflict_equipment.ids.uniq
      # filter the available equipment to not include those which are unavailable
      @equipment = @equipment.where.not(id: unavailable_ids)
    end



    respond_to do |format|
      format.html # index.html.erb
      response.headers['ResultCount'] = @equipment.count
      format.json { render json: @equipment, include: :tags}
    end
  end

  # GET /equipment/1
  # GET /equipment/1.json
  def show
    @equipment = Equipment.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @equipment }
    end
  end


  def future_events
    events = Event.includes(booking: { equipment: :bookings }).where( equipment: { id: params[:id] }).where('start >= ?', Time.now)

    respond_to do |format|
      format.json { render json: events }
    end

  end

  # GET /equipment/new
  # GET /equipment/new.json
  def new
    @equipment = Equipment.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @equipment }
    end
  end

  # GET /equipment/1/edit
  def edit
    @equipment = Equipment.find(params[:id])
  end

  # POST /equipment
  # POST /equipment.json
  def create
    # tags are sent to the controller as a string array
    # we'll create our own way to find the Tag objects
    tags = equipment_params[:tags]

    tags_object_collection = []
    tags&.each do |tag|
      next if tag == ''

      tag_obj = Tag.find(tag)

      tags_object_collection.push(tag_obj) unless tag_obj.nil?
    end

    new_equipment_params = equipment_params.except(:tags)

    @equipment = Equipment.new(new_equipment_params)
    @equipment.tags = tags_object_collection
    respond_to do |format|
      if @equipment.save
        format.html { redirect_to root_path, notice: 'Equipment was successfully created.' }
        format.json { render json: @equipment, status: :created, location: @equipment }
      else
        puts @equipment.inspect
        format.html { render action: 'new' }
        format.json { render json: @equipment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /equipment/1
  # PUT /equipment/1.json
  def update
    tags = equipment_params[:tags]

    tags_object_collection = []
    tags&.each do |tag|
      next if tag == ''

      tag_obj = Tag.find(tag)

      tags_object_collection.push(tag_obj) unless tag_obj.nil?
    end

    new_equipment_params = equipment_params.except(:tags)
    new_equipment_params[:updated_at] = Time.now
    @equipment = Equipment.find(params[:id])
    @equipment.tags = tags_object_collection

    respond_to do |format|
      if @equipment.update(new_equipment_params)
        format.html { redirect_to root_path, notice: 'Equipment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @equipment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /equipment/1
  # DELETE /equipment/1.json
  def destroy
    @equipment = Equipment.find(params[:id])
    @equipment.destroy

    respond_to do |format|
      format.html { redirect_to root_url }
      format.json { head :no_content }
    end
  end

  # Download as Excel
  def download_excel
    @equipment = Equipment.all.includes(:tags)
    respond_to do |format|
      format.xlsx {
        response.headers['Content-Disposition'] = "attachment; filename=items.xlsx"
      }
    end
  end

  private

  def equipment_params
    params.require(:equipment).permit(:name, :barcode, :stored, :contents, :serial_number, :status, tags: [])
  end
end
