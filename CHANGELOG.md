# Changelog
All notable changes will be documented in this file, organized by tagged versions.

## Unreleased

### Added

### Modified
- Fixed bug related to modifier characters being included when manually typing barcodes [#282](https://git.uwaterloo.ca/mad/mad-booking/-/issues/282)

### Removed

## [v2022.07.1] - 2022-07-29

### Added
- Sort field on user list page [#269](https://git.uwaterloo.ca/mad/mad-booking/-/issues/269)
- Text on booking form that shows total number of items that match selection [#254](https://git.uwaterloo.ca/mad/mad-booking/-/issues/254)

### Modified
- Switch to using `vite_ruby` instead of `webpacker` [#277](https://git.uwaterloo.ca/mad/mad-booking/-/issues/277)
- Modified selfserve disabled page [#126](https://git.uwaterloo.ca/mad/mad-booking/-/issues/126)
- Fixed bug where self-serve validation failed when departments were listed [#275](https://git.uwaterloo.ca/mad/mad-booking/-/issues/275)
- Restrict the ability for self-serve users to edit their own bookings, unless certain conditions are met [#262](https://git.uwaterloo.ca/mad/mad-booking/-/issues/262)

### Removed

## [v2022.05.1] - 2022-05-26

### Added

- Hover text on show/edit/delete buttons in reservation list [#256](https://git.uwaterloo.ca/mad/mad-booking/-/issues/256), [#248](https://git.uwaterloo.ca/mad/mad-booking/-/issues/248)
- Expandable panels on dashboard to show equipment [#266](https://git.uwaterloo.ca/mad/mad-booking/-/issues/266)
- Users can now delete their own self-serve bookings [#118](https://git.uwaterloo.ca/mad/mad-booking/-/issues/118)
- Title on booking page [#252](https://git.uwaterloo.ca/mad/mad-booking/-/issues/252)
- Instructions for finding equipment on booking page [#240](https://git.uwaterloo.ca/mad/mad-booking/-/issues/240), [#235](https://git.uwaterloo.ca/mad/mad-booking/-/issues/235)

### Changed

- Updated from Rails 5.2 to 6.1 [#212](https://git.uwaterloo.ca/mad/mad-booking/-/issues/212), [#171] (https://git.uwaterloo.ca/mad/mad-booking/-/issues/171)
- Replaced date/time selector (no longer supported) with native controls [#259](https://git.uwaterloo.ca/mad/mad-booking/-/issues/259)
- Fixed bug related to open/closed hours being interpreted incorrectly [#230](https://git.uwaterloo.ca/mad/mad-booking/-/issues/230)
- More human-readable error messages on conflicts and open hour errors [#260](https://git.uwaterloo.ca/mad/mad-booking/-/issues/260), [#246] (https://git.uwaterloo.ca/mad/mad-booking/-/issues/264)
- Fix bug where last item couldn't be removed from booking [#249](https://git.uwaterloo.ca/mad/mad-booking/-/issues/249)
- Fixed bug where items couldn't be removed from booking if clicking minus icon [#241](https://git.uwaterloo.ca/mad/mad-booking/-/issues/241)
- Fixed bug where approval/rejection of self-serve bookings didn't work [#250](https://git.uwaterloo.ca/mad/mad-booking/-/issues/250)
- Fixed bug where typing enter in search form would save booking [#244](https://git.uwaterloo.ca/mad/mad-booking/-/issues/244)
- Aligned link names on sidebar with page titles [#236](https://git.uwaterloo.ca/mad/mad-booking/-/issues/236)
- Upcoming bookings on dashboard now show all bookings within a day [#234](https://git.uwaterloo.ca/mad/mad-booking/-/issues/234)
- Items overdue for more than a year no longer show up on dashboard [#233](https://git.uwaterloo.ca/mad/mad-booking/-/issues/233)
- Fixed bug that showed wrong times on booking list view [#232](https://git.uwaterloo.ca/mad/mad-booking/-/issues/232)
- Fixed bug where new bookings for new users wouldn't allow saving [#231](https://git.uwaterloo.ca/mad/mad-booking/-/issues/231)

## Removed
- Ability for self-serve users to scan equipment in/out [#251](https://git.uwaterloo.ca/mad/mad-booking/-/issues/251)
